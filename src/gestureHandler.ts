import "hammerjs"
import { Point } from "pixi.js";
import { World } from "./world";

export class GestureHandler{

    hammer: HammerManager

    initialWorldPos = new Point()
    initialWorldScale = new Point()

    constructor(element: HTMLElement, world: World){
        this.hammer = new Hammer(element)

        this.hammer.get("pan").set({direction: Hammer.DIRECTION_ALL})
        this.hammer.get("pinch").set({enable: true})

        this.hammer.on("pan", event => {
            world.x = this.initialWorldPos.x + event.deltaX
            world.y = this.initialWorldPos.y + event.deltaY
        })

        this.hammer.on("panstart", event => {
            this.initialWorldPos.copy(world.position)
            console.log(world)
        })

        this.hammer.on("pinchstart", event => {
            const local = world.parent.toLocal(new Point(event.center.x, event.center.y))

            world.parent.x += local.x
            world.parent.y += local.y

            world.anchor.x += (local.x - world.x) / world.width
            world.anchor.y += (local.y - world.y) / world.height

            world.position.set(0, 0)

            
            world.ground.position.set(
                -world.anchor.x * world.texture.width,
                -world.anchor.y * world.texture.height
            )

            this.initialWorldPos.copy(world.position)
            this.initialWorldScale.copy(world.scale)
        })

        this.hammer.on("pinch", event => {
            world.x = this.initialWorldPos.x + event.deltaX
            world.y = this.initialWorldPos.y + event.deltaY
            
            world.scale.x = this.initialWorldScale.x * event.scale
            world.scale.y = this.initialWorldScale.y * event.scale
        })
    }

}