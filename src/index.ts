import { Application, loader } from "pixi.js";
import { World } from "./world";
import { GestureHandler } from "./gestureHandler";
import { Wall } from "./objects/wall";

export const px = devicePixelRatio || 1

export const app = new Application({
    antialias: false,
    resolution: px
})
app.renderer.backgroundColor = 0xeeeeee
app.renderer.autoResize = true
app.renderer.resize(window.innerWidth, window.innerHeight)

document.body.appendChild(app.view)


let world: World
let gHandler: GestureHandler

const gameLoop = (delta: number) => {
    
}

const setup = () => {
    world = new World()
    app.stage.addChild(world.parent)

    gHandler = new GestureHandler(app.view, world)

    const wall = new Wall()
    world.ground.addChild(wall)

    app.ticker.add(delta => gameLoop(delta))
}

loader
    .add("world", "images/grid.svg")
    .add("pepper", "images/pepper.png")
    .add("pepper2", "images/pepper2.png")
    .add("wall", "images/wall.png")
    .load(setup)