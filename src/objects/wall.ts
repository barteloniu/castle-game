import { Sprite, loader } from "pixi.js";

export class Wall extends Sprite{
    constructor(){
        super(loader.resources["wall"].texture)
    }
}