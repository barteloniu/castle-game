import { Sprite, loader, Container } from "pixi.js";

export class World extends Sprite{

    ground: Container

    constructor(){
        super(loader.resources["world"].texture)

        this.anchor.set(0.5, 0.5)

        const parent = new Container()
        parent.position.set(window.innerWidth / 2, window.innerHeight / 2)
        parent.addChild(this)

        this.ground = new Container()
        this.ground.position.set(
            -this.texture.width / 2,
            -this.texture.height / 2
        )
        this.addChild(this.ground)
    }

}